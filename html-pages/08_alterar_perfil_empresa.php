<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
require_once "includes/id_empresa.php";

$busca = $banco->query("SELECT * FROM cad_empresa AS cad JOIN login ON cad.email = login.email WHERE id_empresa = '$id_empresa'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Alterar Perfil</title>
</head>
<body>
    <header>
        <a href="./11_perfil_empresa.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./14_painel_vagas_empresa.php">Acompanhe suas vagas</a>
                    <a href="./17_tela_vaga_empresa.php">Nova candidatura na sua vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./14_painel_vagas_empresa.php">Minhas Vagas</a>
                    <a href="./20_cadastrar_vaga_empresa.php">Cadastrar Vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-empresa.jpg" class="imagem_topo">
        </section>
        
        <section>
        <?php
            echo"
            <form action='includes/editar_perfil_empresa.php' method='post' class='container_principal' enctype='multipart/form-data'>";
            $reg = $busca->fetch_object();

            echo"
            <div class='imagem_lateral'>
                <h1 class='titulo_pagina'>Editar Perfil</h1>

                <div class='container_img_perfil'>
                    <img src='./fotos/$reg->img_perfil' class='foto_pag_perfil'>
                    <input type='file' class='arquivo_pag_perfil' id='foto_perfil' name='foto_perfil'>
                </div>

                <div class='btn_cadastro_editar'>
                    <a href='11_perfil_empresa.php' class='btn_geral btn_cancelar'>Cancelar</a>
                    <input type='submit' value='Salvar Alterações' class='btn_geral btn_salvar'>
                </div>
            </div>

            <div class='dados_perfil'>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações de Login</h3>
                        
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                            <p>E-mail</p>    
                            <p class='campo_visualizar'>$reg->email</p>
                            </div>
                            <div class='sub_nome'>
                                <label for='senha'>Senha</label>
                                <input type='text' name='senha_login' id='senha_login' class='campo' value='$reg->senha_login'>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Razão Social e Dados de Contato</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='razao_social'>Razão Social</label>
                                <input type='text' name='razao_social' id='razao_social' class='campo' value='$reg->razao_social'>
                            </div>
                            <div class='sub_nome'>
                                <label for='cnpj'>CNPJ</label>
                                <input type='text' name='cnpj' id='cnpj' class='campo' value='$reg->cnpj'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='cidade'>Cidade</label>
                                <input type='text' name='cidade' id='cidade' class='campo' value='$reg->cidade'>
                            </div>
                            <div class='sub_nome'>
                                <label for='site'>Site</label>
                                <input type='url' name='site' id='site' class='campo' value='$reg->site'>
                            </div>
                        </div>

                        <br>
                        
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='telefone'>Telefone</label>
                                <input type='tel' name='telefone' id='telefone' class='campo' value='$reg->telefone'>
                            </div>
                            <div class='sub_nome'>
                                <label for='linkedin'>LinkedIn</label>
                                <input type='text' name='linkedin' id='linkedin' class='campo' value='$reg->linkedin'>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Descrição da Empresa</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <textarea name='descricao' id='descricao' cols='30' rows='10' class='campo'>$reg->descricao</textarea>
                        </div>
                    </div>
                </div>
            </div>
            </form>";
            ?>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>