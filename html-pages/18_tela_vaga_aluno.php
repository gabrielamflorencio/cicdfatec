<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
require_once "includes/id_aluno.php";

$c = $_GET['id_vaga'] ?? 0;
$busca = $banco->query("SELECT * FROM vagas JOIN cad_empresa ON vagas.id_empresa = cad_empresa.id_empresa WHERE vagas.id_vaga = '$c'");
$busca2 = $banco->query("SELECT * FROM candidatura WHERE id_vaga = '$c' AND id_aluno = $id_aluno");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Informações da Vaga</title>
</head>
<body>
    <header>
        <a href="./12_perfil_aluno.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./15_painel_vagas_aluno.php">Nova vaga cadastrada!</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Parabéns! Você foi aprovado</a>
                    <a href="./09_alterar_perfil_aluno.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./15_painel_vagas_aluno.php">Painel de Vagas</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Minhas Candidaturas</a>
                    <a href="./09_alterar_perfil_aluno.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-aluno.jpg" class="imagem_topo">
        </section>
        <?php

        if(!$busca){
            echo "Busca falhou";
        }
        else{
            if($busca->num_rows == 1){
                $reg = $busca->fetch_object();
                $data = data_convert($reg->data_limite);
                echo "

        <section class='container_titulo_vaga'>
            <div class='texto_titulo_vaga'>
                <h1 class='titulo_vaga'>$reg->titulo</h1>
                <h2 class='tipo_vaga'>Área: $reg->area_interesse</h2>
            </div>
            <img src='../img/icones/mesa.png' class='icone_mesa'>
        </section>

        <section class='container_descricao_vaga'>
            <div class='container_info_gerais'>
                <div class='info_gerais_lados'>
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/pin.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->modalidade</h3>
                    </div>

                    <div class='info_gerais_linha'>
                        <img src='../img/icones/bandeira.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->cidade_vaga</h3>
                    </div>
                    
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/pasta.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->tipo_vaga</h3>
                    </div>
                </div>

                <div class='info_gerais_lados'>
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/bolsadinheiro.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->faixa_salarial</h3>
                    </div>

                    <div class='info_gerais_linha'>
                        <img src='../img/icones/relogio.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->carga_horaria</h3>
                    </div>
                    
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/calendario.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>até $data</h3>
                    </div>
                </div>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Empresa Contratante</strong></h2>
                <p class='texto_vaga'><a href='./24_visualizar_perfil_empresa_aluno.php?id_empresa=$reg->id_empresa'>$reg->razao_social</a></p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Descrição da Vaga</strong></h2>
                <p class='texto_vaga'>$reg->descricao_cargo</p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Requisitos da Vaga</strong></h2>
                <p>$reg->habilidade</p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Diferenciais da Vaga</strong></h2>
                <p>$reg->diferenciais</p>
            </div>";
            if($busca2->num_rows == 1){
                $reg2 = $busca2->fetch_object();
                $data_candidatura = data_convert($reg2->data_candidatura);
                echo "<p><strong>Você se candidadou para essa vaga em $data_candidatura</strong></p>";} 
                else {
                    echo"
            <a href='./includes/conecta_aluno.php?id_vaga=$c'><button type='button' class='btn_candidatar'>Candidatar-se a vaga</button></a>
        </section>";
            }
        }
        }
        ?>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>