<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";

$id_vaga = $_GET['id_vaga'] ?? 0;
$busca = $banco->query("SELECT * FROM vagas JOIN cad_empresa ON vagas.id_empresa = cad_empresa.id_empresa WHERE vagas.id_vaga = '$id_vaga'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Informações da Vaga</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-fatec.jpg" class="imagem_topo">
        </section>

        <?php

        if(!$busca){
            echo "Busca falhou";
        }
        else{
            if($busca->num_rows == 1){
                $reg = $busca->fetch_object();
                $data = data_convert($reg->data_limite);
                echo "

        <section class='container_titulo_vaga'>
            <div class='texto_titulo_vaga'>
                <h1 class='titulo_vaga'>$reg->titulo</h1>
                <h2 class='tipo_vaga'>Área: $reg->area_interesse</h2>
            </div>
            <img src='../img/icones/mesa.png' class='icone_mesa'>
        </section>

        <section class='container_descricao_vaga'>
            <div class='container_info_gerais'>
                <div class='info_gerais_lados'>
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/pin.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->modalidade</h3>
                    </div>

                    <div class='info_gerais_linha'>
                        <img src='../img/icones/bandeira.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->cidade_vaga</h3>
                    </div>
                    
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/pasta.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->tipo_vaga</h3>
                    </div>
                </div>

                <div class='info_gerais_lados'>
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/bolsadinheiro.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->faixa_salarial</h3>
                    </div>

                    <div class='info_gerais_linha'>
                        <img src='../img/icones/relogio.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>$reg->carga_horaria</h3>
                    </div>
                    
                    <div class='info_gerais_linha'>
                        <img src='../img/icones/calendario.png' class='icone_tela_vaga'>
                        <h3 class='texto_icone'>até $data</h3>
                    </div>
                </div>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Empresa Contratante</strong></h2>
                <p class='texto_vaga'><a href='./25_visualizar_perfil_empresa_fatec.php?id_empresa=$reg->id_empresa'>$reg->razao_social</a></p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Descrição da Vaga</strong></h2>
                <p class='texto_vaga'>$reg->descricao_cargo</p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Requisitos da Vaga</strong></h2>
                <p>$reg->habilidade</p>
            </div>

            <div class='descricao_vaga'>
                <h2 class='topico_vaga'><strong>Diferenciais da Vaga</strong></h2>
                <p>$reg->diferenciais</p>
            </div>";
            
            if ($reg->status == 1){

            echo"

            <div class='btn_vagas'>
                <a href='includes/arquivar_vaga_fatec.php?id_vaga=$id_vaga'><button type='button' class='btn_pag_vaga arquivar'>Arquivar Vaga</button></a>
                <a href='includes/excluir_vaga_fatec.php?id_vaga=$id_vaga'><button type='button' class='btn_pag_vaga excluir'>Excluir Vaga</button></a>
            </div>
        </section>";}
            
            else {
                echo"

            <div class='btn_vagas'>
                <a href='includes/aprovar_vaga_fatec.php?id_vaga=$id_vaga'><button type='button' class='btn_pag_vaga aprovar'>Aprovar Vaga</button></a>
                <a href='includes/reprovar_vaga_fatec.php?id_vaga=$id_vaga'><button type='button' class='btn_pag_vaga excluir'>Reprovar Vaga</button></a>
            </div>
        </section>";
            }
            
            if ($reg->status == 1){

            echo"

            <h1 class='titulo_candidatos'>Candidatos a vaga</h1>";}}}
                
            if ($reg->status == 1){

        $busca2 = $banco->query("SELECT * FROM candidatura JOIN cad_aluno ON candidatura.id_aluno = cad_aluno.id_aluno WHERE candidatura.id_vaga = '$id_vaga'");
                if(!$busca2){
                    echo "<p>Infelizmente a busca deu errado</p>";
                }
                else{
                if($busca2->num_rows == 0){
                    echo "<p><strong>Não há candidatos para essa vaga no momento</strong></p>";
                }
                else{
                    echo"<section class='container_candidatos'>";

        while ($reg2=$busca2->fetch_object()){
            if ($reg2->status == 1){
            echo "
            <a href='./23_visualizar_perfil_aluno_fatec.php?id_aluno=$reg2->id_aluno' class='candidato_vaga'>
                <img src='./fotos/$reg2->img_perfil' class='foto_candidato'>
                <p class='nome_candidato'><strong>$reg2->nome $reg2->sobrenome</strong></p>
            </a>";}
        }
        echo"</section>";
    }
}}
    
    ?>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>