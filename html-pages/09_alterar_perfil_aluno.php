<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
require_once "includes/id_aluno.php";

$busca = $banco->query("SELECT * FROM cad_aluno AS ca JOIN login AS l ON ca.email = l.email  WHERE id_aluno = '$id_aluno'");

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Alterar perfil</title>
</head>
<body>
    <header>
        <a href="./12_perfil_aluno.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./15_painel_vagas_aluno.php">Nova vaga cadastrada!</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Parabéns! Você foi aprovado</a>
                    <a href="./09_alterar_perfil_aluno.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./15_painel_vagas_aluno.php">Painel de Vagas</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Minhas Candidaturas</a>
                    <a href="./09_alterar_perfil_aluno.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-aluno.jpg" class="imagem_topo">
        </section>
        
        <section>
        <?php
            
            echo"
            <form action='includes/alterar_perfil_aluno.php' method='post' class='container_principal' enctype='multipart/form-data'>";
            $reg = $busca->fetch_object();
            
            echo"
            <div class='imagem_lateral'>
                <h1 class='titulo_pagina'>Editar Perfil</h1>

                <div class='container_img_perfil'>
                    <img src='./fotos/$reg->img_perfil' class='foto_pag_perfil'>
                    <input type='file' class='arquivo_pag_perfil' id='foto_perfil' name='foto_perfil'>
                </div>

                <div class='container_curriculo'>
                    <h2 class='texto_curriculo'>Adicionar currículo</h2>
                    <input type='file'>
                </div>

                <div class='container_descricao'>
                    <h2 class='texto_descricao'>Objetivo</h2>
                    <select name='objetivo' id='objetivo' class='campo'>
                        <option value='$reg->objetivo'>$reg->objetivo</option>
                        <option value='Administração'>Administração</option>
                        <option value='Desenvolvimento de Softwares'>Desenvolvimento de Softwares</option>
                        <option value='Gestão da Produção Industrial'>Gestão da Produção Industrial</option>
                        <option value='Gestão Empresarial'>Gestão Empresarial</option>
                        <option value='Gestão da Tecnologia da Informação'>Gestão da Tecnologia da Informação</option>
                        <option value='Infraestrutura de TI'>Infraestrutura de TI</option>
                        <option value='Segurança'>Segurança</option>
                    </select>
                </div>

                <div class='btn_cadastro_editar'>
                    <a href='12_perfil_aluno.php' class='btn_geral btn_cancelar'>Cancelar</a>
                    <input type='submit' value='Salvar Alterações' class='btn_geral btn_salvar'>
                </div>
            </div>

            <div class='dados_perfil'>
                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações de Login</h3>
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>E-mail</p>
                                <p class='campo_visualizar'>$reg->email</p>
                            </div>
                            <div class='sub_nome'>
                                <label for='senha_login'>Senha</label>
                                <input type='text' name='senha_login' id='senha_login' value='$reg->senha_login' class='campo'>
                            </div>
                        </div>
                    </div>   
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações Pessoais e Dados de Contato</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='nome'>Nome</label>
                                <input type='text' name='nome' id='nome' value='$reg->nome' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='sobrenome'>Sobrenome</label>
                                <input type='text' name='sobrenome' id='sobrenome' value='$reg->sobrenome' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='cpf'>CPF</label>
                                <input type='text' name='cpf' id='cpf' value='$reg->cpf' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='data_nascimento'>Data de Nascimento</label>
                                <input type='date' name='data_nascimento' id='data_nascimento' value='$reg->data_nascimento' class='campo'>
                            </div>
                        </div>

                        <br>
                        
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='telefone'>Telefone</label>
                                <input type='telefone' name='telefone' id='telefone' value='$reg->telefone'  class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='celular'>Celular</label>
                                <input type='tel' name='celular' id='celular' value='$reg->celular' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='cidade'>Cidade</label>
                                <input type='text' name='cidade' id='cidade' value='$reg->cidade' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>                            
                            <div class='sub_nome'>
                                <label for='github'>GitHub</label>
                                <input type='url' name='github' id='github' value='$reg->github' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='linkedin'>LinkedIn</label>
                                <input type='url' name='linkedin' id='linkedin' value='$reg->linkedin' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>                            
                            <div class='sub_nome'>
                                <label for='pretensao_salarial'>Pretensão salarial</label>
                                <input type='text' name='pretensao_salarial' id='pretensao_salarial' value='$reg->pretensao_salarial' class='campo'>
                            </div>

                            <div class='sub_nome'>
                                <label for='modalidade'>Modalidade de preferência</label>
                                <select name='modalidade' id='modalidade' value='$reg->modalidade' class='campo'>
                                    <option value='Híbrida'>Híbrida</option>
                                    <option value='Home-office'>Home-office</option>
                                    <option value='Presencial'>Presencial</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações Acadêmicas</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='ra'>RA</label>
                                <input type='text' name='ra' id='ra' value='$reg->ra' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='curso'>Curso</label>
                                <select name='curso' id='curso' value='$reg->curso' class='campo'>
                                    <option value='Desenvolvimento de Software Multiplataformas'>Desenvolvimento de Software Multiplataformas</option>
                                    <option value='Gestão da Produção Industrial'>Gestão da Produção Industrial</option>
                                    <option value='Gestão da Tecnologia da Informação'>Gestão da Tecnologia da Informação</option>
                                    <option value='Gestão Empresarial'>Gestão Empresarial</option>
                                </select>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='data_inicio_fatec'>Data de início na Fatec</label>
                                <input type='date' name='data_inicio_fatec' id='data_inicio_fatec' value='$reg->data_inicio_fatec' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='data_previsao_termino'>Previsão de conclusão do curso</label>
                                <input type='date' name='data_previsao_termino' id='data_previsao_termino' value='$reg->data_previsao_termino' class='campo'>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <div class='formulario'>
                        <h3 class='subtitulo'>Habilidades Técnicas</h3>  
                        <br>
                        <textarea name='habilidades' id='habilidades' cols='30' rows='10' class='campo'>$reg->habilidades</textarea>                                                 
                    </div>   
                </div>
            </div>
        </form>";
        ?>
            
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>