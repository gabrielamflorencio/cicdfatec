<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Cadastrar Vaga</title>
</head>
<body>
    <header>
        <a href="./11_perfil_empresa.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./14_painel_vagas_empresa.php">Acompanhe suas vagas</a>
                    <a href="./17_tela_vaga_empresa.php">Nova candidatura na sua vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./14_painel_vagas_empresa.php">Minhas Vagas</a>
                    <a href="./20_cadastrar_vaga_empresa.php">Cadastrar Vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-empresa.jpg" class="imagem_topo">
        </section>

        <section>
            <form action="includes/cadastro_vaga.php" method="post" class="container_principal">
            <div class="imagem_lateral">
                <h1 class="titulo_pagina">Cadastrar vaga</h1>

                <div class="container_descricao">
                    <h2 class="texto_descricao">Descrição do cargo</h2>
                    <textarea name="descricao_cargo" id="descricao_cargo" cols="30" rows="30" class="area_descricao"></textarea>
                </div>

                <div class="btn_cadastro_editar">
                    <input type="reset" value="Cancelar" class="btn_geral btn_cancelar">
                    <input type="submit" value="Submeter Cadastro" class="btn_geral btn_salvar">
                </div>
            </div>

            <div class="dados_perfil">

                <div class="card_perfil">
                    <h3 class="subtitulo">Informações Gerais</h3>
                    
                    <div class="formulario">

                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="titulo">Título do Cargo</label>
                                <input type="text" name="titulo" id="titulo" class="campo">
                            </div>
                            <div class="sub_nome">
                                <label for="data_limite">Data Limite para Candidaturas</label>
                                <input type="date" name="data_limite" id="data_limite" class="campo">
                            </div>
                        </div>

                        <br>

                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="area_interesse">Área da Vaga</label>
                                <select name="area_interesse" id="area_interesse" class="campo">
                                    <option value="Administração">Administração</option>
                                    <option value="Desenvolvimento de Softwares">Desenvolvimento de Softwares</option>
                                    <option value="Gestão da Produção Industrial">Gestão da Produção Industrial</option>
                                    <option value="Gestão Empresarial">Gestão Empresarial</option>
                                    <option value="Gestão da Tecnologia da Informação">Gestão da Tecnologia da Informação</option>
                                    <option value="Infraestrutura de TI">Infraestrutura de TI</option>
                                    <option value="Segurança">Segurança</option>
                                </select>
                            </div>
                            <div class="sub_nome">
                                <label for="modalidade">Modalidade de Trabalho</label>
                                <select name="modalidade" id="modalidade" class="campo">
                                    <option value="Híbrida">Híbrida</option>
                                    <option value="Home-office">Home-office</option>
                                    <option value="Presencial">Presencial</option>
                                </select>
                            </div>
                        </div>

                        <br>
                        
                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="tipo_vaga">Tipo de Contrato</label>
                                <select name="tipo_vaga" id="tipo_vaga" class="campo">
                                    <option value="Contrato de Trabalho (PJ)">Contrato de Trabalho (PJ)</option>
                                    <option value="Contratação efetiva (CLT)">Contratação efetiva (CLT)</option>
                                    <option value="Estágio de Férias">Estágio de Férias</option>
                                    <option value="Estágio Regular">Estágio Regular</option>
                                    <option value="Freelance">Freelance</option>
                                    <option value="Trainee">Trainee</option>
                                </select>
                            </div>
                            <div class="sub_nome">
                                <label for="cidade_vaga">Cidade da Vaga</label>
                                <input type="text" name="cidade_vaga" id="cidade_vaga" class="campo">
                            </div>
                        </div>

                        <br>

                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="faixa_salarial">Faixa Salarial</label>
                                <input type="text" name="faixa_salarial" id="faixa_salarial" class="campo">
                            </div>
                            <div class="sub_nome">
                                <label for="carga_horaria">Carga horária</label>
                                <input type="text" name="carga_horaria" id="carga_horaria" class="campo">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card_perfil">
                    <h3 class="subtitulo">Requisitos Mínimos</h3>
                    
                    <div class="formulario">
                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <textarea name="habilidade" id="habilidade" cols="30" rows="10" class="campo"></textarea>
                            </div>
                          
                        </div>
                    </div>
                </div>

                <div class="card_perfil">
                    <h3 class="subtitulo">Diferenciais</h3>
                    
                    <div class="formulario">
                        <div class="sub_formulario">
                            <textarea name="diferenciais" id="diferenciais" cols="30" rows="10" class="campo"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>