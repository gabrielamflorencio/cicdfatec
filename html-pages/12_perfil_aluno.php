<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
require_once "includes/id_aluno.php";

$busca = $banco->query("SELECT * FROM cad_aluno AS cad JOIN login ON cad.email = login.email WHERE id_aluno = '$id_aluno'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Meu Perfil</title>
</head>
<body>
    <header>
        <a href="./12_perfil_aluno.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./15_painel_vagas_aluno.php">Nova vaga cadastrada!</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Parabéns! Você foi aprovado</a>
                    <a href="./09_alterar_perfil_aluno.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./15_painel_vagas_aluno.php">Painel de Vagas</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Minhas Candidaturas</a>
                    <a href="./09_alterar_perfil_aluno.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-aluno.jpg" class="imagem_topo">
        </section>

        <section class="resumo_perfil">
            <?php

            if(!$busca){
                echo "Você não está logado";
            }
            else{
                if($busca->num_rows == 1){
                    $reg = $busca->fetch_object();
                    echo "
                        <p class='nome_perfil'>Olá, $reg->nome</p>
              
                        <div class='info_perfil'>
                            <div class='container_foto'>
                                <img src='./fotos/$reg->img_perfil' class='foto_perfil'>
                                <a href='./09_alterar_perfil_aluno.php' class='editar_perfil'>Editar Perfil</a>
                            </div>";
                }
            }
            ?>

                <div class="area_info_card">
                    <div class="info_card">
                        <img src="../img/icones/dinheiro.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Pretensão Salarial</h3>
                        <p class="texto_info_card">É importante que você informe quanto gostaria de ganhar por mês para que as empresas te ofereçam um salário de acordo com a sua necessidade</p>
                        <a href="./09_alterar_perfil_aluno.php" class="link_info_card">Atualizar pretensão salarial</a>
                    </div>

                    <div class="info_card">
                        <img src="../img/icones/pin.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Modalidade de Trabalho</h3>
                        <p class="texto_info_card">É importante que você informe o tipo de modalidade (presencial, home-office ou híbrido) que atualmente consegue se enquadrar na sua realidade</p>
                        <a href="./09_alterar_perfil_aluno.php" class="link_info_card">Atualizar modalidade de trabalho</a>
                    </div>

                    <div class="info_card">
                        <img src="../img/icones/pasta.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Perfil de Vagas</h3>
                        <p class="texto_info_card">É importante que você informe qual o perfil de vagas que te interessa para que possamos selecionar as melhores oportunidades que se enquadram no seus objetivos</p>
                        <a href="./09_alterar_perfil_aluno.php" class="link_info_card">Atualizar perfil de vagas</a>
                    </div>
                    <div class="info_card">
                        <img src="../img/icones/chapeuformatura.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Formação e Cursos</h3>
                        <p class="texto_info_card">Adicione o seu nível de escolaridade e os cursos que você realizou. As empresas usam essa informação para saber seus conhecimentos técnicos</p>
                        <a href="./09_alterar_perfil_aluno.php" class="link_info_card">Atualizar formação e cursos</a>
                    </div>
                </div>
            </div>

        </section>

        <h2 class="texto_mini_painel">De acordo com o seu perfil, essas vagas são a sua cara</h2>

        <section class="mini_painel">
            <div class="area_cards">
            <?php
                $busca2 =  $banco->query("SELECT * FROM vagas WHERE area_interesse = '$reg->objetivo' ORDER BY data_limite");
                if(!$busca2){
                    echo "<p class='link_painel_vagas'>Infelizmente a busca deu errado</p>";
                }
                else{
                if($busca2->num_rows == 0){
                    echo "<p class='link_painel_vagas'>Não há vagas no seu perfil no momento!</p>";
                }
                else{
                    for($i=0; $i<3; $i++){
                        $reg2=$busca2->fetch_object();
                        if($reg2->status == 1){
                            $data_limite = data_convert($reg2->data_limite);
                                echo"
                                    <div class='card_vaga'>
                                        <div class='head_card_vaga'>
                                            <h3 class='titulo_card_vaga'> $reg2->titulo</h3>
                                            <p class='area_card_vaga'>Área: $reg2->area_interesse</p>
                                        </div>

                                        <div class='body_card_vaga'>
                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/pin.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->modalidade</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/bandeira.png'   class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->cidade_vaga</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/pasta.png'  class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->tipo_vaga</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/bolsadinheiro.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->faixa_salarial</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/calendario.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>até $data_limite</p>
                                            </div>

                                            <a href='./18_tela_vaga_aluno.php?id_vaga=$reg2->id_vaga' class='link_vaga'>+ informações</a>

                                        </div>
                                    </div>";
                        }
                    }
                }
                
            }
            ?>
            </div>
            
            <a href="./15_painel_vagas_aluno.php" class="link_painel_vagas">ver mais vagas</a>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>