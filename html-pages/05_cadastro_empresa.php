<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Cadastro Empresa</title>
</head>
<body>
    <header>
        <a href="./01_home.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>

        <nav>
            <div class="dropdown">
                <button class="dropbtn-home">Cadastrar-se</button>

                <div class="dropdown-content content-sino btn-home">
                    <a href="./06_cadastro_aluno.php">Aluno</a>
                    <a href="./05_cadastro_empresa.php">Empresa</a>
                    <a href="./07_cadastro_fatec.php">Fatec</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn-home">Login</button>
                
                <div class="dropdown-content btn-home">
                    <a href="./03_login_aluno.php">Aluno</a>
                    <a href="./02_login_empresa.php">Empresa</a>
                    <a href="./04_login_fatec.php">Fatec</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-geral.jpg" class="imagem_topo">
        </section>
        
        <section>
            <form action="includes/cadastro_empresa.php" method="POST" class="container_principal" enctype="multipart/form-data"><!--Abrir um form a partir daqui-->
                <div class="imagem_lateral">
                    <h1 class="titulo_pagina">Cadastrar Empresa</h1>

                    <div class="container_img_perfil">
                        <img src="../img/user.jpg" class="foto_pag_perfil">
                        <input type="file" class="arquivo_pag_perfil" id="foto_perfil" name="foto_perfil">
                    </div>

                    <div class="btn_cadastro_editar">
                        <input type="reset" value="Cancelar" class="btn_geral btn_cancelar">
                        <input type="submit" value="Cadastrar" class="btn_geral btn_salvar">
                    </div>
                </div>

                <div class="dados_perfil">

                    <div class="card_perfil">
                        <h3 class="subtitulo">Informações de Login</h3>
                        
                        <div class="formulario"> <!--Trocar o form action por uma div e 
                                                            utilizar a classe "formulário"-->
                            <div class="sub_formulario">
                                <div class="sub_nome">
                                    <label for="email">E-mail</label>
                                    <input type="email" name="email" id="email" class="campo">
                                </div>
                                <div class="sub_nome">
                                    <label for="senha">Senha</label>
                                    <input type="text" name="senha_login" id="senha_login" class="campo">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card_perfil">
                        <h3 class="subtitulo">Razão Social e Dados de Contato</h3>
                        
                        <div class="formulario"><!--Trocar o form action por uma div e 
                                                            utilizar a classe "formulário"-->
                            <div class="sub_formulario">
                                <div class="sub_nome">
                                    <label for="razao_social">Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" class="campo">
                                </div>
                                <div class="sub_nome">
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="campo">
                                </div>
                            </div>

                            <br>

                            <div class="sub_formulario">
                                <div class="sub_nome">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" name="cidade" id="cidade" class="campo">
                                </div>
                                <div class="sub_nome">
                                    <label for="site">Site</label>
                                    <input type="url" name="site" id="site" class="campo">
                                </div>
                            </div>

                            <br>
                            
                            <div class="sub_formulario">
                                <div class="sub_nome">
                                    <label for="telefone">Telefone</label>
                                    <input type="tel" name="telefone" id="telefone" class="campo">
                                </div>
                                <div class="sub_nome">
                                    <label for="linkedin">LinkedIn</label>
                                    <input type="text" name="linkedin" id="linkedin" class="campo">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card_perfil">
                        <h3 class="subtitulo">Descrição da Empresa</h3>
                        
                        <div class="formulario"><!--Trocar o form action por uma div e 
                                                            utilizar a classe "formulário"-->
                            <div class="sub_formulario">
                                <textarea name="descricao" id="descricao" cols="30" rows="10" class="campo"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>