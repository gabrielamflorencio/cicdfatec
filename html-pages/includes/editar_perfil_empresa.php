<?php
require_once "conexao.php";
require_once "id_empresa.php";

$busca = $banco->query("SELECT * FROM cad_empresa AS cad JOIN login ON cad.email = login.email WHERE id_empresa = '$id_empresa'");
$reg = $busca->fetch_object();

$email = $reg->email;

$senha_login = $_POST['senha_login'];
$razao_social = $_POST['razao_social'];
$cnpj = $_POST['cnpj'];
$cidade = $_POST['cidade'];
$site = $_POST['site'];
$telefone = $_POST['telefone'];
$linkedin = $_POST['linkedin'];
$descricao = $_POST['descricao'];
$foto_perfil = $_FILES['foto_perfil'];

if ($foto_perfil != NULL){
    $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
    $new_name = $razao_social. $ext; //Definindo um novo nome para o arquivo
    $dir = '../fotos/'; //Diretório para uploads 
    move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
    $img_perfil = $new_name;
}

$sql1 = "UPDATE login SET senha_login = ? WHERE email = '$email'";

$sql2 = "UPDATE cad_empresa SET email = ?, razao_social = ?, cnpj = ?, cidade = ?, site = ?, telefone = ?, linkedin = ?, descricao = ?, img_perfil = ? WHERE id_empresa = '$id_empresa'";

$stmt_edit1 = $banco->prepare($sql1);

$stmt_edit1->bind_param("s",$senha_login);

$stmt_edit2 = $banco->prepare($sql2);

$stmt_edit2->bind_param("sssssssss",$email, $razao_social, $cnpj, $cidade, $site, $telefone, $linkedin, $descricao, $img_perfil);


if ($stmt_edit1->execute()){
    if($stmt_edit2->execute()){
    echo "<script> alert ('Cadastro alterado com sucesso!'); location.href=('../11_perfil_empresa.php')</script>";
}
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../08_alterar_perfil_empresa.php')</script>";
}

$banco->close();
