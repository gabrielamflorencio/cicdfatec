<?php
require_once "conexao.php";

$id_aluno = $_GET['id_aluno'] ?? 0;

$status = 1;


$sql = "UPDATE cad_aluno SET status = ? WHERE id_aluno = '$id_aluno'";

$stmt_aprovar = $banco->prepare($sql);

$stmt_aprovar->bind_param("i",$status);


if ($stmt_aprovar->execute()){
    echo "<script> alert ('Cadastro de aluno aprovado com sucesso!'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}

$banco->close();
