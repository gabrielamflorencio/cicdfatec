<?php
require_once "conexao.php";
require_once "id_aluno.php";

$busca = $banco->query("SELECT * FROM cad_aluno AS ca JOIN login AS l ON ca.email = l.email  WHERE id_aluno = '$id_aluno'");
$reg = $busca->fetch_object();


$email=$reg->email;

$objetivo = $_POST['objetivo'];
$senha_login = $_POST['senha_login'];
$nome = $_POST['nome'];
$sobrenome = $_POST['sobrenome'];
$cpf = $_POST['cpf'];
$data_nascimento = $_POST['data_nascimento'];
$telefone = $_POST['telefone'];
$celular = $_POST['celular'];
$cidade = $_POST['cidade'];
$github = $_POST['github'];
$linkedin = $_POST['linkedin'];
$pretensao_salarial = $_POST['pretensao_salarial'];
$modalidade = $_POST['modalidade'];
$ra = $_POST['ra'];
$curso = $_POST['curso'];
$data_inicio_fatec = $_POST['data_inicio_fatec'];
$data_previsao_termino = $_POST['data_previsao_termino'];
$habilidades = $_POST['habilidades'];
$foto_perfil = $_FILES['foto_perfil'];

if ($foto_perfil != NULL){
    $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
    $new_name = $cpf. $ext; //Definindo um novo nome para o arquivo
    $dir = '../fotos/'; //Diretório para uploads 
    move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
    $img_perfil = $new_name;
}


$sql = "UPDATE login SET senha_login = ? WHERE email = '$email'";

  $stmt = $banco->prepare($sql);

  $stmt->bind_param("s",$senha_login);


if ($stmt->execute()){

    $sql2 = "UPDATE cad_aluno SET objetivo = ?, nome = ?, sobrenome = ?, cpf = ?, data_nascimento = ?, telefone = ?, celular = ?, cidade = ?, github = ?, linkedin = ?, pretensao_salarial = ?, modalidade = ?, ra = ?, curso = ?, data_inicio_fatec = ?,  data_previsao_termino = ?, habilidades = ?, img_perfil = ? WHERE id_aluno = '$id_aluno'";

    $stmt2 = $banco->prepare($sql2);

    $stmt2->bind_param("ssssssssssssssssss", $objetivo, $nome, $sobrenome, $cpf, $data_nascimento, $telefone, $celular, $cidade, $github, $linkedin, $pretensao_salarial, $modalidade, $ra,  $curso, $data_inicio_fatec, $data_previsao_termino, $habilidades, $img_perfil);


    if ($stmt2->execute()){
        echo "<script> alert ('Cadastro alterado com Sucesso!'); location.href=('../12_perfil_aluno.php')</script>";
    }
    else {
        echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../09_alterar_perfil_aluno.php')</script>";
    }
}
else{
    echo"<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../09_alterar_perfil_aluno.php')</script>";
}

$banco->close();