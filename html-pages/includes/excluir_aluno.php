<?php
require_once "conexao.php";

$id_aluno = $_GET['id_aluno'] ?? 0;

$status = 3;


$sql = "UPDATE cad_aluno SET status = ? WHERE id_aluno = '$id_aluno'";

$stmt_excluir = $banco->prepare($sql);

$stmt_excluir->bind_param("i",$status);


if ($stmt_excluir->execute()){
    echo "<script> alert ('Cadastro de aluno excluído com sucesso!'); location.href=('../26_alunos_cadastrados_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../23_visualizar_perfil_aluno.php?id_aluno=$id_aluno')</script>";
}

$banco->close();
