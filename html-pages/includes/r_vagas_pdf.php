
    
    <?php
    
    require_once "conexao.php";

	// Carregar o Composer
	require "vendor/autoload.php";

    $arquivo = 'rel.pdf';
    $html = '';
    $html .= '<table border="1" >';
    $html .= '<table layout = "auto">';
    $html .= '<table width = "100%">';
    

	$arquivo = 'rel.xls';
    $html = '';
    $html .= '<table border="1">';

	$html .= '<tr>';
    $html .= '<td> ID';
	$html .= '<td> ID Empresa';
	$html .= '<td> Data Limite';
	$html .= '<td> Cidade';
    $html .= '<td> Carga Horária';
	$html .= '<td> Descrição Cargo';
	$html .= '<td> Faixa Salarial';
	$html .= '<td> Habiilidade';
	$html .= '<td> Área Interesse';
	$html .= '<td> Diferenciais';
	$html .= '<td> Status';
	$html .= '<td> Título';
	$html .= '<td> Modalidade';
	$html .= '<td> Tipo Vaga';

	

    		//Selecionar todos os itens da tabela 
		$busca = "SELECT * FROM vagas";
		$busca = mysqli_query($banco , $busca);
		
		while($row_aluno = mysqli_fetch_assoc($busca)){
			$html .= '<tr>';
			$html .= '<td>'.$row_aluno["id_vaga"].'</td>';
			$html .= '<td>'.$row_aluno["id_empresa"].'</td>';
			$html .= '<td>'.$row_aluno["data_limite"].'</td>';
			$html .= '<td>'.$row_aluno["cidade_vaga"].'</td>';
			$html .= '<td>'.$row_aluno["carga_horaria"].'</td>';
			$html .= '<td>'.$row_aluno["descricao_cargo"].'</td>';
			$html .= '<td>'.$row_aluno["faixa_salarial"].'</td>';
			$html .= '<td>'.$row_aluno["habilidade"].'</td>';
			$html .= '<td>'.$row_aluno["area_interesse"].'</td>';
			$html .= '<td>'.$row_aluno["diferenciais"].'</td>';
			$html .= '<td>'.$row_aluno["status"].'</td>';
			$html .= '<td>'.$row_aluno["titulo"].'</td>';
			$html .= '<td>'.$row_aluno["modalidade"].'</td>';
			$html .= '<td>'.$row_aluno["tipo_vaga"].'</td>';
			
			$html .= '</tr>';			
		}


// Referenciar o namespace Dompdf
use Dompdf\Dompdf;

// Instanciar e usar a classe dompdf
$dompdf = new Dompdf(['enable_remote' => true]);

// Instanciar o metodo loadHtml e enviar o conteudo do PDF
$dompdf->loadHtml($html);

// Configurar o tamanho e a orientacao do papel
// landscape - Imprimir no formato paisagem
//$dompdf->setPaper('A4', 'landscape');
// portrait - Imprimir no formato retrato

$dompdf->setPaper('A4', 'landscape');

// Renderizar o HTML como PDF
$dompdf->render();

// Gerar o PDF
$dompdf->stream("",array("Attachment" => false));
