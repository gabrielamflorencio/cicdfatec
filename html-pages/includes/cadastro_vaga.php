<?php
require_once "conexao.php";
require_once "id_empresa.php";


$data_limite = $_POST['data_limite'];
$cidade_vaga = $_POST['cidade_vaga'];
$carga_horaria = $_POST['carga_horaria'];
$descricao_cargo = $_POST['descricao_cargo'];
$faixa_salarial = $_POST['faixa_salarial'];
$habilidade = $_POST['habilidade'];
$area_interesse = $_POST['area_interesse'];
$diferenciais = $_POST['diferenciais'];
$status = 0;
$titulo = $_POST['titulo'];
$modalidade = $_POST['modalidade'];
$tipo_vaga = $_POST['tipo_vaga'];

$sql = "INSERT INTO vagas (id_empresa, data_limite, cidade_vaga, carga_horaria, descricao_cargo, faixa_salarial, habilidade, area_interesse, diferenciais, status, titulo, modalidade, tipo_vaga) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

$stmt = $banco->prepare($sql);

$stmt->bind_param("issssssssisss",$id_empresa,$data_limite,$cidade_vaga,$carga_horaria,$descricao_cargo,$faixa_salarial,$habilidade,$area_interesse,$diferenciais,$status,$titulo,$modalidade,$tipo_vaga);


if ($stmt->execute()){
    echo "<script> alert ('Vaga submetida para aprovação!'); location.href=('../11_perfil_empresa.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../20_cadastrar_vaga_empresa.php')</script>";
}

$banco->close();

?>
