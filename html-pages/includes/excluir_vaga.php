<?php
require_once "conexao.php";

$id_vaga = $_GET['id_vaga'] ?? 0;

$status = 3;


$sql = "UPDATE vagas SET status = ? WHERE id_vaga = '$id_vaga'";

$stmt_excluir = $banco->prepare($sql);

$stmt_excluir->bind_param("i",$status);


if ($stmt_excluir->execute()){
    echo "<script> alert ('Vaga excluída com sucesso!'); location.href=('../14_painel_vagas_empresa.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../17_tela_vaga_empresa.php?id_vaga=$id_vaga')</script>";
}

$banco->close();
