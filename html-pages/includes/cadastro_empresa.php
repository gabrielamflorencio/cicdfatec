<?php
  require_once "conexao.php";

  $email = $_POST['email'];
  $senha_login = $_POST['senha_login'];
  $razao_social = $_POST['razao_social'];
  $cnpj = $_POST['cnpj'];
  $cidade = $_POST['cidade'];
  $site = $_POST['site'];
  $telefone = $_POST['telefone'];
  $linkedin = $_POST['linkedin'];
  $tipo = 'empresa';
  $descricao = $_POST['descricao'];
  $status_empresa = 0;

  $foto_perfil = $_FILES['foto_perfil'];

  if ($foto_perfil != NULL){
      $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
      $new_name = $razao_social. $ext; //Definindo um novo nome para o arquivo
      $dir = '../fotos/'; //Diretório para uploads 
      move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
      $img_perfil = $new_name;
      if($ext == NULL){
        $img_perfil = 'user.jpg';
      }
  }

  $sql = "INSERT INTO login (email,senha_login,tipo) VALUES (?,?,?)";

  $stmt = $banco->prepare($sql);

  $stmt->bind_param("sss",$email,$senha_login,$tipo);

  if ($stmt->execute()){

    $sql2 = "INSERT INTO cad_empresa (email, razao_social, cnpj, cidade, site, telefone, linkedin, descricao,status_empresa, img_perfil)
              VALUES (?,?,?,?,?,?,?,?,?,?)";

    $stmt2 = $banco->prepare($sql2);

    $stmt2->bind_param("ssssssssis",$email, $razao_social, $cnpj, $cidade, $site, $telefone, $linkedin, $descricao, $status_empresa, $img_perfil);


    if ($stmt2->execute()){
        echo "<script> alert ('Cadastro submetido para aprovação! Aguarde e-mail!'); location.href=('../01_home.php')</script>";
    }
    else {
        echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../05_cadastro_empresa.php')</script>";
    }
}
else{
    echo"<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../05_cadastro_empresa.php')</script>";
}

$banco->close();

/*$sql = "INSERT INTO cad_empresa (id_empresa, email, senha, razao_social, cnpj, cidade, site, telefone, linkedin, descricao_empresa)
          VALUES (?,?,?,?,?,?,?,?,?,?);";

$stmt = $banco->prepare($sql);

$stmt->bind_param("issssssssisss",$id_empresa,$data_limite,$cidade_vaga,$carga_horaria,$descricao_cargo,$faixa_salarial,$habilidade,
                  $area_interesse,$diferenciais,$status,$titulo,$modalidade,$tipo_vaga);

$stmt->bind_param("issssssssisss",$id_empresa,$data_limite,$cidade_vaga,$carga_horaria,$descricao_cargo,$faixa_salarial,$habilidade,
                  $area_interesse,$diferenciais,$status,$titulo,$modalidade,$tipo_vaga);*/

?>