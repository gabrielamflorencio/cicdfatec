<?php
require_once "conexao.php";

$ra = $_POST['ra'];
$cpf = $_POST['cpf'];
$nome = $_POST['nome'];
$sobrenome = $_POST['sobrenome'];
$data_nascimento = $_POST['data_nascimento'];
$telefone = $_POST['telefone'];
$celular = $_POST['celular'];
$email = $_POST['email'];
$cidade = $_POST['cidade'];
$github = $_POST['github'];
$linkedin = $_POST['linkedin'];
$curso = $_POST['curso'];
$data_inicio_fatec = $_POST['data_inicio_fatec'];
$data_previsao_termino = $_POST['data_previsao_termino'];
$objetivo = $_POST['objetivo'];
$pretensao_salarial = $_POST['pretensao_salarial'];
$habilidades = $_POST['habilidades'];
$modalidade = $_POST['modalidade'];
$tipo = 'aluno';
$senha_login = $_POST['senha_login'];
$status = 0;
$foto_perfil = $_FILES['foto_perfil'];

if ($foto_perfil != NULL){
    $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
    $new_name = $cpf. $ext; //Definindo um novo nome para o arquivo
    $dir = '../fotos/'; //Diretório para uploads 
    move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
    $img_perfil = $new_name;

    if($ext == NULL){
        $img_perfil = 'user.jpg';
    }
}


$sql = "INSERT INTO login (email,senha_login,tipo) VALUES (?,?,?)";

$stmt = $banco->prepare($sql);

$stmt->bind_param("sss",$email,$senha_login,$tipo);

$sql2 = "INSERT INTO cad_aluno (ra, cpf, nome, sobrenome, data_nascimento, telefone,celular, email, cidade, github, linkedin, curso, data_inicio_fatec, data_previsao_termino, objetivo, pretensao_salarial, habilidades,  modalidade, status, img_perfil) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

$stmt2 = $banco->prepare($sql2);

$stmt2->bind_param("sssssssssssssssissis",$ra,$cpf,$nome,$sobrenome,$data_nascimento,$telefone,$celular,$email,$cidade,$github,$linkedin,$curso,$data_inicio_fatec,$data_previsao_termino,$objetivo,$pretensao_salarial,$habilidades,$modalidade, $status, $img_perfil);

if ($stmt->execute()){

    if ($stmt2->execute()){
        echo "<script> alert ('Cadastro submetido para aprovação! Aguarde e-mail!'); location.href=('../01_home.php')</script>";
    }
    else {
        echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../06_cadastro_aluno.php')</script>";
    }
}
else{
    echo"<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../06_cadastro_aluno.php')</script>";
}

$banco->close();

?>