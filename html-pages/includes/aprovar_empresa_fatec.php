<?php
require_once "conexao.php";

$id_empresa = $_GET['id_empresa'] ?? 0;

$status_empresa = 1;


$sql = "UPDATE cad_empresa SET status_empresa = ? WHERE id_empresa = '$id_empresa'";

$stmt_aprovar = $banco->prepare($sql);

$stmt_aprovar->bind_param("i",$status_empresa);


if ($stmt_aprovar->execute()){
    echo "<script> alert ('Empresa aprovada com sucesso!'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}

$banco->close();
