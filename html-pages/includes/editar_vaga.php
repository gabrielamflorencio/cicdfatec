<?php
require_once "conexao.php";

$id_vaga = $_GET['id_vaga'] ?? 0;

$data_limite = $_POST['data_limite'];
$cidade_vaga = $_POST['cidade_vaga'];
$carga_horaria = $_POST['carga_horaria'];
$descricao_cargo = $_POST['descricao_cargo'];
$faixa_salarial = $_POST['faixa_salarial'];
$habilidade = $_POST['habilidade'];
$area_interesse = $_POST['area_interesse'];
$diferenciais = $_POST['diferenciais'];
$status = 0;
$titulo = $_POST['titulo'];
$modalidade = $_POST['modalidade'];
$tipo_vaga = $_POST['tipo_vaga'];

$sql = "UPDATE vagas SET data_limite = ?, cidade_vaga = ?, carga_horaria = ?, descricao_cargo = ?, faixa_salarial = ?, habilidade = ?, area_interesse = ?, diferenciais = ?, titulo = ?, modalidade = ?, tipo_vaga = ? WHERE id_vaga = '$id_vaga'";

$stmt_edit = $banco->prepare($sql);

$stmt_edit->bind_param("sssssssssss",$data_limite,$cidade_vaga,$carga_horaria,$descricao_cargo,$faixa_salarial,$habilidade,$area_interesse,$diferenciais,$titulo,$modalidade,$tipo_vaga);


if ($stmt_edit->execute()){
    echo "<script> alert ('Vaga alterada com sucesso!'); location.href=('../17_tela_vaga_empresa.php?id_vaga=$id_vaga')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../17_tela_vaga_empresa.php?id_vaga=$id_vaga')</script>";
}

$banco->close();
