<?php
require_once "conexao.php";

$id_empresa = $_GET['id_empresa'] ?? 0;

$status = 4;


$sql = "UPDATE cad_empresa SET status_empresa = ? WHERE id_empresa = '$id_empresa'";

$stmt_reprovar = $banco->prepare($sql);

$stmt_reprovar->bind_param("i",$status);


if ($stmt_reprovar->execute()){
    echo "<script> alert ('Cadastro reprovado com sucesso!'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}

$banco->close();
