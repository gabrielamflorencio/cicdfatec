
    
    <?php
    
    require_once "conexao.php";

	// Carregar o Composer
	require "vendor/autoload.php";

    $arquivo = 'rel.pdf';
    $html = '';
    $html .= '<table border="0" >';
    $html .= '<table layout = "auto">';
    $html .= '<table width = "100%">';
    

	$arquivo = 'rel.xls';
    $html = '';
    $html .= '<table border="1">';

	$html .= '<tr>';
    $html .= '<td> ID';
	$html .= '<td> CNPJ';
	$html .= '<td> Razão Social';
	$html .= '<td> Telefone';
    $html .= '<td> Email';
	$html .= '<td> Cidade';
	$html .= '<td> Site';
	$html .= '<td> Linkedin';
	$html .= '<td> Descrição';
	$html .= '<td> Status';
    $html .= '<\tr>';
	

    		//Selecionar todos os itens da tabela 
		$busca = "SELECT * FROM cad_empresa";
		$busca = mysqli_query($banco , $busca);
		
		while($row_aluno = mysqli_fetch_assoc($busca)){
			$html .= '<tr>';
			$html .= '<td>'.$row_aluno["id_empresa"].'</td>';
			$html .= '<td>'.$row_aluno["cnpj"].'</td>';
			$html .= '<td>'.$row_aluno["razao_social"].'</td>';
			$html .= '<td>'.$row_aluno["telefone"].'</td>';
			$html .= '<td>'.$row_aluno["email"].'</td>';
			$html .= '<td>'.$row_aluno["cidade"].'</td>';
			$html .= '<td>'.$row_aluno["site"].'</td>';
			$html .= '<td>'.$row_aluno["linkedin"].'</td>';
			$html .= '<td>'.$row_aluno["descricao"].'</td>';
			$html .= '<td>'.$row_aluno["status_empresa"].'</td>';

			
			$html .= '</tr>';			
		}


// Referenciar o namespace Dompdf
use Dompdf\Dompdf;

// Instanciar e usar a classe dompdf
$dompdf = new Dompdf(['enable_remote' => true]);

// Instanciar o metodo loadHtml e enviar o conteudo do PDF
$dompdf->loadHtml($html);

// Configurar o tamanho e a orientacao do papel
// landscape - Imprimir no formato paisagem
//$dompdf->setPaper('A4', 'landscape');
// portrait - Imprimir no formato retrato

$dompdf->setPaper('A4', 'landscape');

// Renderizar o HTML como PDF
$dompdf->render();

// Gerar o PDF
$dompdf->stream("",array("Attachment" => false));
