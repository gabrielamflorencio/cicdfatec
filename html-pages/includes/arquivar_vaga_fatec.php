<?php
require_once "conexao.php";

$id_vaga = $_GET['id_vaga'] ?? 0;

$status = 2;


$sql = "UPDATE vagas SET status = ? WHERE id_vaga = '$id_vaga'";

$stmt_arquivar = $banco->prepare($sql);

$stmt_arquivar->bind_param("i",$status);


if ($stmt_arquivar->execute()){
    echo "<script> alert ('Vaga arquivada com sucesso!'); location.href=('../16_painel_vagas_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../19_tela_vaga_fatec.php?id_vaga=$id_vaga')</script>";
}

$banco->close();
