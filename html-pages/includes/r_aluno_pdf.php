
    
    <?php
    
    require_once "conexao.php";

	// Carregar o Composer
	require "vendor/autoload.php";

    $arquivo = 'rel.pdf';
    $html = '';
    $html .= '<table border="1" >';
    $html .= '<table layout = "auto">';
    $html .= '<table width = "100%">';
    
    $arquivo = 'rel.xls';
    $html = '';
    $html .= '<table border="1">';

    $html .= '<tr>';
    $html .= '<td> ID';
    $html .= '<td> RA';
	$html .= '<td> CPF';
	$html .= '<td> Nome';
	$html .= '<td> Sobrenome';
	$html .= '<td> Data Nascimento';
	$html .= '<td> Telefone';
	$html .= '<td> Celular';
    $html .= '<td> Email';
	$html .= '<td> Cidade';
	$html .= '<td> Github';
	$html .= '<td> Linkedin';
	$html .= '<td> Curso';
	$html .= '<td> Data Início Curso';
	$html .= '<td> Data Previsão Término';
	$html .= '<td> Objetivo';
	$html .= '<td> Pretensão Salarial';
	$html .= '<td> Habilidades';
	$html .= '<td> modalidade';


    		//Selecionar todos os itens da tabela 
		$busca = "SELECT * FROM cad_aluno";
		$busca = mysqli_query($banco , $busca);
		
		while($row_aluno = mysqli_fetch_assoc($busca)){
			$html .= '<tr>';
			$html .= '<td>'.$row_aluno["id_aluno"].'</td>';
			$html .= '<td>'.$row_aluno["ra"].'</td>';
			$html .= '<td>'.$row_aluno["cpf"].'</td>';
			$html .= '<td>'.$row_aluno["nome"].'</td>';
			$html .= '<td>'.$row_aluno["sobrenome"].'</td>';
			$html .= '<td>'.$row_aluno["data_nascimento"].'</td>';
			$html .= '<td>'.$row_aluno["telefone"].'</td>';
			$html .= '<td>'.$row_aluno["celular"].'</td>';
			$html .= '<td>'.$row_aluno["email"].'</td>';
			$html .= '<td>'.$row_aluno["cidade"].'</td>';
			$html .= '<td>'.$row_aluno["github"].'</td>';
			$html .= '<td>'.$row_aluno["linkedin"].'</td>';
			$html .= '<td>'.$row_aluno["curso"].'</td>';
			$html .= '<td>'.$row_aluno["data_inicio_fatec"].'</td>';
			$html .= '<td>'.$row_aluno["data_previsao_termino"].'</td>';
			$html .= '<td>'.$row_aluno["objetivo"].'</td>';
			$html .= '<td>'.$row_aluno["pretensao_salarial"].'</td>';
			$html .= '<td>'.$row_aluno["habilidades"].'</td>';
			$html .= '<td>'.$row_aluno["modalidade"].'</td>';
			
			$html .= '</tr>';
			;

			
		}


// Referenciar o namespace Dompdf
use Dompdf\Dompdf;

// Instanciar e usar a classe dompdf
$dompdf = new Dompdf(['enable_remote' => true]);

// Instanciar o metodo loadHtml e enviar o conteudo do PDF
$dompdf->loadHtml($html);

// Configurar o tamanho e a orientacao do papel
// landscape - Imprimir no formato paisagem
//$dompdf->setPaper('A4', 'landscape');
// portrait - Imprimir no formato retrato

$dompdf->setPaper('A4', 'landscape');

// Renderizar o HTML como PDF
$dompdf->render();

// Gerar o PDF
$dompdf->stream("",array("Attachment" => false));
