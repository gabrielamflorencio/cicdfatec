<?php
require_once "includes/conexao.php";
require_once "includes/login.php";
?>

<?php

if (empty($_SESSION['user'])){
echo "

<header>
<a href='./01_home.html'><img src='../img/logo_fatec.png' class='logo_fatec' id='topo'></a>

<nav>
    <div class='dropdown'>
        <button class='dropbtn-home'>Cadastrar-se</button>

        <div class='dropdown-content content-sino btn-home'>
            <a href='./06_cadastro_aluno.html'>Aluno</a>
            <a href='./05_cadastro_empresa.html'>Empresa</a>
            <a href='./07_cadastro_fatec.html'>Fatec</a>
        </div>
    </div>

    <div class='dropdown'>
        <button class='dropbtn-home'>Login</button>
        
        <div class='dropdown-content btn-home'>
            <a href='./03_login_aluno.php'>Aluno</a>
            <a href='./02_login_empresa.html'>Empresa</a>
            <a href='./04_login_fatec.html'>Fatec</a>
        </div>
    </div>
</nav>
</header>

";
} else {

    if (is_empresa()) {

   echo '
   <header>
   <a href="./01_home.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
   <nav>
       <div class="dropdown">
           <button class="dropbtn">
               <img src="../img/icones/sino.png" class="header_icone">
           </button>

           <div class="dropdown-content content-sino">
               <a href="./14_painel_vagas_empresa.html">Acompanhe suas vagas</a>
               <a href="./17_tela_vaga_empresa.html">Nova candidatura na sua vaga</a>
               <a href="./08_alterar_perfil_empresa.html">Atualize seu perfil</a>
           </div>
       </div>

       <div class="dropdown">
           <button class="dropbtn">
               <img src="../img/icones/menu.png" class="header_icone">
           </button>
           
           <div class="dropdown-content">
               <a href="./14_painel_vagas_empresa.html">Minhas Vagas</a>
               <a href="./20_cadastrar_vaga_empresa.html">Cadastrar Vaga</a>
               <a href="./08_alterar_perfil_empresa.html">Alterar Perfil</a>
               <a href="03_login_aluno_logout.php">Sair</a>
           </div>
       </div>
   </nav>
   </header>';
    }
        else if (is_aluno()) { 
        echo ' 
    <header>
    <a href="./01_home.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>    
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./15_painel_vagas_aluno.html">Nova vaga cadastrada!</a>
                    <a href="./29_minhas_candidaturas_aluno.html">Parabéns! Você foi aprovado</a>
                    <a href="./09_alterar_perfil_aluno.html">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./15_painel_vagas_aluno.html">Painel de Vagas</a>
                    <a href="./29_minhas_candidaturas_aluno.html">Minhas Candidaturas</a>
                    <a href="./09_alterar_perfil_aluno.html">Alterar Perfil</a>
                    <a href="03_login_aluno_logout.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>';

    }

} 
?>
