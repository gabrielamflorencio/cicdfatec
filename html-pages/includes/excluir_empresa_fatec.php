<?php
require_once "conexao.php";

$id_empresa = $_GET['id_empresa'] ?? 0;

$status = 3;


$sql = "UPDATE cad_empresa SET status_empresa = ? WHERE id_empresa = '$id_empresa'";

$stmt_reprovar = $banco->prepare($sql);

$stmt_reprovar->bind_param("i",$status);


if ($stmt_reprovar->execute()){
    echo "<script> alert ('Cadastro excluído com sucesso!'); location.href=('../27_empresas_cadastradas_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../25_visualizar_perfil_empresa_fatec.php?id_empresa=$id_empresa')</script>";
}

$banco->close();
