<?php
require_once "conexao.php";

$id_vaga = $_GET['id_vaga'] ?? 0;

$status = 1;


$sql = "UPDATE vagas SET status = ? WHERE id_vaga = '$id_vaga'";

$stmt_arquivar = $banco->prepare($sql);

$stmt_arquivar->bind_param("i",$status);


if ($stmt_arquivar->execute()){
    echo "<script> alert ('Vaga aprovada com sucesso!'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}

$banco->close();
