<?php
require_once "conexao.php";
require_once "id_aluno.php";

$id_vaga = $_GET["id_vaga"];

$sql = "INSERT INTO candidatura (id_vaga, id_aluno) VALUES (?,?)";

$stmt = $banco->prepare($sql);

$stmt->bind_param("ii",$id_vaga,$id_aluno);


if ($stmt->execute()){
    echo "<script> alert ('Candidatura efetuada com sucesso!'); location.href=('../18_tela_vaga_aluno.php?id_vaga=$id_vaga')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../18_tela_vaga_aluno.php?id_vaga=$id_vaga')</script>";
}

$banco->close();

?>