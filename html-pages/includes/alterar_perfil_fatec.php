<?php

  require_once "conexao.php";
  require_once "id_fatec.php";

  $busca = $banco->query("SELECT * FROM cad_fatec AS ca JOIN login AS l ON ca.email = l.email WHERE id_fatec = '$id_fatec'");
  $reg = $busca->fetch_object();

  $email = $reg->$email;

  $senha_login = $_POST['senha_login'];
  $nome = $_POST['nome'];
  $sobrenome = $_POST['sobrenome'];
  $cpf = $_POST['cpf'];
  $data_nascimento = $_POST['data_nascimento'];
  $telefone = $_POST['telefone'];
  $celular = $_POST['celular'];
  $cargo = $_POST['cargo'];
  $foto_perfil = $_FILES['foto_perfil'];

if ($foto_perfil != NULL){
  $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
  $new_name = $cpf. $ext; //Definindo um novo nome para o arquivo
  $dir = '../fotos/'; //Diretório para uploads 
  move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
  $img_perfil = $new_name;
}

  $sql = "UPDATE login SET senha_login = ? WHERE email = '$email'";
  $stmt = $banco->prepare($sql);

  $stmt->bind_param("s",$senha_login);

  if ($stmt->execute()){

    $sql2 = "UPDATE cad_fatec SET nome = ?, sobrenome = ?, cpf = ?, data_nascimento = ?, telefone = ?, celular = ?, cargo = ?, img_perfil = ? WHERE id_fatec = '$id_fatec'";
    $stmt2 = $banco->prepare($sql2);

    $stmt2->bind_param("ssssssss", $nome, $sobrenome, $cpf, $data_nascimento, $telefone, $celular, $cargo, $img_perfil);
    
    if ($stmt2->execute()){
      echo "<script> alert ('Cadastro alterado com Sucesso!'); location.href=('../13_perfil_fatec.php')</script>";
  }
  else {
      echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../10_alterar_perfil_fatec.php')</script>";
  }
}
else{
  echo"<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../10_alterar_perfil_fatec.php')</script>";
}

$banco->close();
?>