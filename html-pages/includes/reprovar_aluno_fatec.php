<?php
require_once "conexao.php";

$id_aluno = $_GET['id_aluno'] ?? 0;

$status = 4;


$sql = "UPDATE cad_aluno SET status = ? WHERE id_aluno = '$id_aluno'";

$stmt_reprovar = $banco->prepare($sql);

$stmt_reprovar->bind_param("i",$status);


if ($stmt_reprovar->execute()){
    echo "<script> alert ('Cadastro de aluno reprovado com sucesso!'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}
else {
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../28_solicitacao_aprovacao_fatec.php')</script>";
}

$banco->close();
