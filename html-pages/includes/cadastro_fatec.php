<?php

require_once "conexao.php";

$nome = $_POST['nome'];
$sobrenome = $_POST['sobrenome'];
$cpf = $_POST['cpf'];
$data_nascimento = $_POST['data_nascimento'];
$telefone = $_POST['telefone'];
$celular = $_POST ['celular'];
$email = $_POST ['email'];
$cargo = $_POST ['cargo'];
$senha_login = $_POST ['senha_login'];
$tipo = 'fatec';
$status = 0;
$foto_perfil = $_FILES['foto_perfil'];

if ($foto_perfil != NULL){
  $ext = strtolower(substr($_FILES['foto_perfil']['name'],-4)); //Pegando extensão do arquivo
  $new_name = $nome. $ext; //Definindo um novo nome para o arquivo
  $dir = '../fotos/'; //Diretório para uploads 
  move_uploaded_file($_FILES['foto_perfil']['tmp_name'], "$dir$new_name"); //Fazer upload do arquivo
  $img_perfil = $new_name;
  
  if($ext == NULL){
    $img_perfil = 'user.jpg';
  }
}


$sql = "INSERT INTO login (email,senha_login,tipo) VALUES (?,?,?)";

$stmt = $banco->prepare($sql);

$stmt->bind_param("sss",$email,$senha_login,$tipo);

if ($stmt->execute()){

  $sql2 = "INSERT INTO cad_fatec (nome, sobrenome, cpf, data_nascimento, telefone, celular, email, cargo, status, img_perfil) VALUES (?,?,?,?,?,?,?,?,?,?)";

  $stmt2 = $banco->prepare($sql2);

  $stmt2->bind_param("ssssssssis",$nome, $sobrenome, $cpf, $data_nascimento, $telefone, $celular, $email, $cargo,$status,$img_perfil);

  if ($stmt2->execute()){
    echo "<script> alert ('Cadastro submetido para aprovação! Aguarde e-mail!'); location.href=('../01_home.php')</script>";
  }
  else{
    echo "<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../07_cadastro_fatec.php')</script>";
  }
}
else{
  echo"<script> alert ('Algo deu errado! Tente novamente mais tarde'); location.href=('../07_cadastro_fatec.php')</script>";
}

$banco->close();