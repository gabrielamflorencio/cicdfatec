<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Relatório Gerencial</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        

        <section class="texto_intro">
            <table >
                <tr><td> <strong><h2>Relatório</h2></strong> <td> <strong><h2>Download</h2></strong>
                <tr><td>Alunos cadastrados <td>
                    <a href="includes/r_aluno_excel.php" target="_blank"><img src="../img/icones/xls.png" alt="excel" class="icone-relatorio"></a>
                    <a href="includes/r_aluno_pdf.php" target="_blank"><img src="../img/icones/pdf.png" alt="pdf" class="icone-relatorio"></a>
                <tr><td>Empresas cadastradas <td>
                    <a href="includes/r_empresa_excel.php" target="_blank"><img src="../img/icones/xls.png" alt="excel" class="icone-relatorio"></a>
                    <a href="includes/r_empresa_pdf.php" target="_blank"><img src="../img/icones/pdf.png" alt="pdf" class="icone-relatorio"></a>
                <tr><td>Vagas  <td>
                    <a href="includes/r_vagas_excel.php" target="_blank"><img src="../img/icones/xls.png" alt="excel" class="icone-relatorio"></a>
                    <a href="includes/r_vagas_pdf.php" target="_blank"><img src="../img/icones/pdf.png" alt="pdf" class="icone-relatorio"></a>
            </table>
        </section>

    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>