<?php
    require_once "includes/conexao.php";
    require_once "includes/funcoes.php";
    require_once "includes/id_fatec.php";

    $busca = $banco->query("SELECT * FROM cad_fatec AS ca JOIN login AS l ON ca.email = l.email  WHERE id_fatec = '$id_fatec'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Alterar Perfil</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-fatec.jpg" class="imagem_topo">
        </section>
        
        <section>
        <?php 
        
            echo "<form action='includes/alterar_perfil_fatec.php' method='post' class='container_principal' enctype='multipart/form-data'>";
            $reg = $busca->fetch_object();

            echo"
            <div class='imagem_lateral'>
                <h1 class='titulo_pagina'>Editar Perfil</h1>

                <div class='container_img_perfil'>
                    <img src='./fotos/$reg->img_perfil' class='foto_pag_perfil'>
                    <input type='file' class='arquivo_pag_perfil' id='foto_perfil' name='foto_perfil'>
                </div>

                <div class='btn_cadastro_editar'>
                    <a href='13_perfil_fatec.php' class='btn_geral btn_cancelar'>Cancelar</a>
                    <input type='submit' value='Salvar Alterações' class='btn_geral btn_salvar'>
                </div>
            </div>

            <div class='dados_perfil'>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações de Login</h3>
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>E-mail</p>
                                <p class='campo_visualizar'>$reg->email</p>
                            </div>
                            <div class='sub_nome'>
                                <label for='senha_login'>Senha</label>
                                <input type='text' name='senha_login' id='senha_login' value='$reg->senha_login' class='campo'>
                            </div>
                        </div>
                    </div>   
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações Pessoais e Dados de Contato</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='nome'>Nome</label>
                                <input type='text' name='nome' id='nome' value='$reg->nome' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='sobrenome'>Sobrenome</label>
                                <input type='text' name='sobrenome' id='sobrenome' value='$reg->sobrenome' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='cpf'>CPF</label>
                                <input type='text' name='cpf' id='cpf' value='$reg->cpf' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='data_nascimento'>Data de Nascimento</label>
                                <input type='date' name='data_nascimento' id='data_nascimento' value='$reg->data_nascimento' class='campo'>
                            </div>
                        </div>

                        <br>
                        
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='telefone'>Telefone</label>
                                <input type='tel' name='telefone' id='telefone' value='$reg->telefone' class='campo'>
                            </div>
                            <div class='sub_nome'>
                                <label for='celular'>Celular</label>
                                <input type='tel' name='celular' id='celular' value='$reg->celular' class='campo'>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <label for='cargo'>Cargo / Função</label>
                                <input type='text' name='cargo' id='cargo' value='$reg->cargo' class='campo'>
                            </div>
                        </div>
                    </div>
                </div>
            </form>";
            ?>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>