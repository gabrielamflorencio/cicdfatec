<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Empresas Cadastradas</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-fatec.jpg" class="imagem_topo">
        </section>

        <section class="texto_intro pag-cadastro">
            <h2>Acompanhe de perto!</h2>
            <p>Estas são as empresas já cadastrados e aprovadas.</p>
        </section>

        <section class="painel-cadastros-cards">
        <?php
                
                $busca1 =  $banco->query("SELECT * FROM cad_empresa WHERE status_empresa = 1");
                if(!$busca1){
                    echo "<p>Infelizmente a busca deu errado</p>";
                }
                else{
                    if($busca1->num_rows == 0){
                        echo "<p>Não há empresas cadastradas e aprovadas!</p>";
                    }
                    else{
                        while ($reg1=$busca1->fetch_object()){
                            echo"
                            <div class='card-cadastros'>
                                <div class='area-foto-btn'>
                                    <img src='./fotos/$reg1->img_perfil' class='foto-cadastro'>
                                    <a href='./25_visualizar_perfil_empresa_fatec.php?id_empresa=$reg1->id_empresa'><button type='button' class='btn_cadastro visualizar-perfil'>Perfil</button></a>
                                    <a href='includes/excluir_empresa_fatec.php?id_empresa=$reg1->id_empresa'><button type='button' class='btn_cadastro excluir'>Excluir</button></a>
                                </div>
                                <div class='area-cadastro-info'>
                                    <div class='sub_nome'>
                                        <p><strong>Razão Social</strong></p>
                                        <p class='campo_visualizar'>$reg1->razao_social</p>
                                    </div>
                                    <div class='sub_nome'>
                                        <p><strong>CNPJ</strong></p>
                                        <p class='campo_visualizar'>$reg1->cnpj</p>
                                    </div>
                                    <div class='sub_nome'>
                                        <p><strong>E-mail</strong></p>
                                        <p class='campo_visualizar'>$reg1->email</p>
                                    </div>
                                    <div class='sub_nome'>
                                        <p><strong>Site</strong></p>
                                        <p class='campo_visualizar'>$reg1->site</p>
                                    </div>
                                </div>
                            </div>

                        </div>";
                        }
                    }
                }
            ?>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>