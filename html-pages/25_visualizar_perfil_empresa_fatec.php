<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";

$id_empresa = $_GET['id_empresa'] ?? 0;
$busca = $banco->query("SELECT * FROM cad_empresa WHERE id_empresa = '$id_empresa'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Informações da Empresa</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-fatec.jpg" class="imagem_topo">
        </section>

        <?php

        if(!$busca){
            echo "Busca falhou";
        }
        else{
            if($busca->num_rows == 1){
                $reg = $busca->fetch_object();
                echo "
        <section class='container_principal'>
            <div class='imagem_lateral'>
                <h1 class='titulo_pagina'>Perfil da Empresa</h1>

                <div class='container_img_perfil'>
                    <img src='./fotos/$reg->img_perfil' class='foto_pag_perfil'>
                </div>

                <div class='btn_cadastro_editar'>";
                    if($reg->status_empresa == 0){
                    echo"
                    <a href='includes/aprovar_empresa_fatec.php?id_empresa=$id_empresa'><button type='button' class='btn_geral aprovar'>Aprovar Cadastro</button></a>
                    <a href='includes/reprovar_empresa_fatec.php?id_empresa=$id_empresa'><button type='button' class='btn_geral btn_excluir'>Reprovar Cadastro</button></a>";
                    }
                    else{
                    echo"
                    <a href='includes/excluir_empresa_fatec.php?id_empresa=$id_empresa'><button type='button' class='btn_geral btn_excluir'>Excluir Cadastro</button></a>";}

                    echo"
                </div>
            </div>

            <div class='dados_perfil'>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Razão Social e Dados de Contato</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Razão Social</p>
                                <p class='campo_visualizar'>$reg->razao_social</p>
                            </div>
                            <div class='sub_nome'>
                                <p>CNPJ</p>
                                <p class='campo_visualizar'>$reg->cnpj</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Cidade</p>
                                <p class='campo_visualizar'>$reg->cidade</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Site</p>
                                <p class='campo_visualizar'><a href='$reg->site' target='_blank'>$reg->site</a></p>
                            </div>
                        </div>

                        <br>
                        
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Telefone</p>
                                <p class='campo_visualizar'>$reg->telefone</p>
                            </div>
                            <div class='sub_nome'>
                                <p>E-mail</p>
                                <p class='campo_visualizar'>$reg->email</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Descrição da Empresa</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <p class='area_descricao_visualizar'>$reg->descricao</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>";
        }
    }
    ?>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>