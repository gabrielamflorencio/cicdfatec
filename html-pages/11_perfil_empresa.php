<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
require_once "includes/id_empresa.php";

$busca = $banco->query("SELECT * FROM cad_empresa AS cad JOIN login ON cad.email = login.email WHERE id_empresa = '$id_empresa'");
$busca2 =  $banco->query("SELECT * FROM vagas WHERE status = 1 AND id_empresa='$id_empresa' ORDER BY data_limite");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Meu Perfil</title>
</head>
<body>
    <header>
        <a href="./11_perfil_empresa.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./14_painel_vagas_empresa.php">Acompanhe suas vagas</a>
                    <a href="./17_tela_vaga_empresa.php">Nova candidatura na sua vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./14_painel_vagas_empresa.php">Minhas Vagas</a>
                    <a href="./20_cadastrar_vaga_empresa.php">Cadastrar Vaga</a>
                    <a href="./08_alterar_perfil_empresa.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-empresa.jpg" class="imagem_topo">
        </section>

        <section class="resumo_perfil">
        <?php

            if(!$busca){
                echo "Você não está logado";
            }
            else{
                if($busca->num_rows == 1){
                    $reg = $busca->fetch_object();
                    echo "
                        <p class='nome_perfil'>Olá, $reg->razao_social</p>
              
                        <div class='info_perfil'>
                            <div class='container_foto'>
                                <img src='./fotos/$reg->img_perfil' class='foto_perfil'>
                                <a href='./08_alterar_perfil_empresa.php' class='editar_perfil'>Editar Perfil</a>
                            </div>";
                }
            }
        ?>
                <div class="area_info_card">
                    <div class="info_card">
                        <img src="../img/icones/dinheiro.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Faixa Salarial</h3>
                        <p class="texto_info_card">É importante que você informe a faixa salarial correspondente à vaga que está anunciando ou se este ponto será negociado posteriormente entre a empresa e o candidato</p>
                        <a href="./20_cadastrar_vaga_empresa.php" class="link_info_card">Cadastre nova vaga!</a>
                    </div>

                    <div class="info_card">
                        <img src="../img/icones/pin.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Modalidade de Trabalho</h3>
                        <p class="texto_info_card">É importante que você informe ao candidato o tipo de modalidade (presencial, home-office ou híbrido) que atualmente consegue se enquadrar na sua realidade</p>
                        <a href="./20_cadastrar_vaga_empresa.php" class="link_info_card">Cadastre nova vaga!</a>
                    </div>

                    <div class="info_card">
                        <img src="../img/icones/pasta.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Área das Vagas</h3>
                        <p class="texto_info_card">É importante que você informe qual a área correspondente à vaga anunciada (Comercial, Gestão, Administrativo, Tecnologia da Informação)</p>
                        <a href="./20_cadastrar_vaga_empresa.php" class="link_info_card">Cadastre nova vaga!</a>
                    </div>
                    <div class="info_card">
                        <img src="../img/icones/chapeuformatura.png" class="icone_info_card">
                        <h3 class="titulo_info_card">Requsitos da Vaga</h3>
                        <p class="texto_info_card">Adicione os requisitos mínimos para o preenchimento da vaga (formação, cursos, ferramentas, softwares, softskills). Também é interessante acrescentar quais são os possíveis diferenciais</p>
                        <a href="./20_cadastrar_vaga_empresa.php" class="link_info_card">Cadastre nova vaga!</a>
                    </div>
                </div>
            </div>

        </section>
        
        <h2 class="texto_mini_painel">Acompanhe de perto! Essas são suas vagas com prazo mais curto para candidaturas</h2>

        <section class="mini_painel">
            <div class="area_cards">

            <?php
                if(!$busca2){
                    echo "<p class='link_painel_vagas'>Infelizmente a busca deu errado</p>";
                }
                else{
                if($busca2->num_rows == 0){
                    echo "<p class='link_painel_vagas'>Não há vagas cadastradas no momento!</p>";
                }
                else{
                    for($i=0; $i<3; $i++){
                        $reg2=$busca2->fetch_object();
                        if($reg2->status == 1){
                            $data_limite = data_convert($reg2->data_limite);
                                echo"
                                    <div class='card_vaga'>
                                        <div class='head_card_vaga'>
                                            <h3 class='titulo_card_vaga'> $reg2->titulo</h3>
                                            <p class='area_card_vaga'>Área: $reg2->area_interesse</p>
                                        </div>

                                        <div class='body_card_vaga'>
                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/pin.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->modalidade</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/bandeira.png'   class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->cidade_vaga</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/pasta.png'  class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->tipo_vaga</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/bolsadinheiro.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>$reg2->faixa_salarial</p>
                                            </div>

                                            <div class='info_card_vaga'>
                                                <img src='../img/icones/calendario.png' class='icone_card_vaga'>
                                                <p class='descricaco_card_vaga'>até $data_limite</p>
                                            </div>

                                            <a href='./17_tela_vaga_empresa.php?id_vaga=$reg2->id_vaga' class='link_vaga'>+ informações</a>

                                        </div>
                                    </div>";
                        }
                    }
                }
                
            }
            ?>
                
            </div>
            
            <a href="./14_painel_vagas_empresa.php" class="link_painel_vagas">ver mais vagas</a>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>