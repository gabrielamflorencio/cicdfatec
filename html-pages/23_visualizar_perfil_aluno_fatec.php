<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";

$id_aluno = $_GET['id_aluno'] ?? 0;
$busca = $banco->query("SELECT * FROM cad_aluno WHERE id_aluno = '$id_aluno'");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Informações do Aluno</title>
</head>
<body>
    <header>
        <a href="./13_perfil_fatec.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./16_painel_vagas_fatec.php">Nova vaga cadastrada!</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Nova solicitação pendente</a>
                    <a href="./10_alterar_perfil_fatec.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./16_painel_vagas_fatec.php">Painel de Vagas</a>
                    <a href="./27_empresas_cadastradas_fatec.php">Empresas Cadastradas</a>
                    <a href="./26_alunos_cadastrados_fatec.php">Alunos Cadastrados</a>
                    <a href="./28_solicitacao_aprovacao_fatec.php">Solicitações Pendentes</a>
                    <a href="./30_relatorio_gerencial_fatec.php">Relatório Gerencial</a>
                    <a href="./10_alterar_perfil_fatec.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-fatec.jpg" class="imagem_topo">
        </section>

        <?php

        if(!$busca){
            echo "Busca falhou";
        }
        else{
            if($busca->num_rows == 1){
                $reg = $busca->fetch_object();
                $data_nascimento = data_convert($reg->data_nascimento);
                $data_inicio_fatec = data_convert($reg->data_inicio_fatec);
                $data_previsao_termino = data_convert($reg->data_previsao_termino);
                
                echo "
        <section class='container_principal'>
            <div class='imagem_lateral'>
                <h1 class='titulo_pagina'>Aluno(a) Fatec</h1>

                <div class='container_img_perfil'>
                    <img src='./fotos/$reg->img_perfil' class='foto_pag_perfil'>
                </div>

                <div class='container_curriculo'>
                    <a href='#'><button type='button' class='btn_geral btn_cancelar'>Baixar Currículo</button></a>
                </div>

                <div class='container_descricao'>
                    <h2 class='texto_descricao'>Objetivos</h2>
                    <p class='area_descricao_visualizar'>$reg->objetivo</p>
                </div>

                <div class='btn_cadastro_editar'>";
                    if($reg->status == 0){
                        echo"
                        <a href='includes/aprovar_aluno_fatec.php?id_aluno=$id_aluno'><button type='button' class='btn_geral aprovar'>Aprovar Cadastro</button></a>
                        <a href='includes/reprovar_aluno_fatec.php?id_aluno=$id_aluno'><button type='button' class='btn_geral btn_excluir'>Reprovar Cadastro</button></a>";
                    }
                    else{
                    echo"
                    <a href='includes/excluir_aluno.php?id_aluno=$id_aluno'><button type='button' class='btn_geral btn_excluir'>Excluir Cadastro</button></a>";
                    }

                    echo"
                </div>
            </div>

            <div class='dados_perfil'>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações Pessoais e Dados de Contato</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Nome</p>
                                <p class='campo_visualizar'>$reg->nome</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Sobrenome</p>
                                <p class='campo_visualizar'>$reg->sobrenome</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>CPF</p>
                                <p class='campo_visualizar'>$reg->cpf</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Data de nascimento</p>
                                <p class='campo_visualizar'>$data_nascimento</p>
                            </div>
                        </div>

                        <br>
                        
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Telefone</p>
                                <p class='campo_visualizar'>$reg->telefone</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Celular</p>
                                <p class='campo_visualizar'>$reg->celular</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>E-mail</p>
                                <p class='campo_visualizar'>$reg->email</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Cidade</p>
                                <p class='campo_visualizar'>$reg->cidade</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>                            
                            <div class='sub_nome'>
                                <p>GitHub</p>
                                <p class='campo_visualizar'>$reg->github</p>
                            </div>
                            <div class='sub_nome'>
                                <p>LinkedIn</p>
                                <p class='campo_visualizar'>$reg->linkedin</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>                            
                            <div class='sub_nome'>
                                <p>Pretensão salarial</p>
                                <p class='campo_visualizar'>R$ $reg->pretensao_salarial,00</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Modalidade de preferência</p>
                                <p class='campo_visualizar'>$reg->modalidade</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Informações Acadêmicas</h3>
                    
                    <div class='formulario'>
                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>RA</p>
                                <p class='campo_visualizar'>$reg->ra</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Curso</p>
                                <p class='campo_visualizar'>$reg->curso</p>
                            </div>
                        </div>

                        <br>

                        <div class='sub_formulario'>
                            <div class='sub_nome'>
                                <p>Data de início na Fatec</p>
                                <p class='campo_visualizar'>$data_inicio_fatec</p>
                            </div>
                            <div class='sub_nome'>
                                <p>Previsão de conclusão do curso</p>
                                <p class='campo_visualizar'>$data_previsao_termino</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='card_perfil'>
                    <h3 class='subtitulo'>Habilidades Técnicas</h3>
                    
                    <div class='formulario'>
                        <p class='campo_visualizar'>$reg->habilidades</p>
                    </div>
                </div>
            </div>
        </section>";
        }
    }
    ?>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>