<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Home</title>

</head>
<body>
    <header>
        <a href="./01_home.php"><img src="img/logo_fatec.png" class="logo_fatec logo-home" id="topo"></a>

        <nav>
            <div class="dropdown">
                <button class="dropbtn-home">Cadastrar-se</button>

                <div class="dropdown-content drop-home btn-home">
                    <a href="./06_cadastro_aluno.php">Aluno</a>
                    <a href="./05_cadastro_empresa.php">Empresa</a>
                    <a href="./07_cadastro_fatec.php">Fatec</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn-home">Login</button>
                
                <div class="dropdown-content btn-home">
                    <a href="./03_login_aluno.php">Aluno</a>
                    <a href="./02_login_empresa.php">Empresa</a>
                    <a href="./04_login_fatec.php">Fatec</a>
                </div>
            </div>
        </nav>
    </header>
    <main>
    <section class="conteudo_home">
        <div class="container_home_descricao">
            <h1 class="titulo_home">Portal de Vagas</h1>
            <br>
            <br>
            <h3 class="sub_home">Plataforma de vagas exclusiva</h3>
            <p class="texto_home">Somos uma plataforma 100% dedicada ao recrutamento de alunos da Fatec de Itapira.</p>
            <br>
            <h3 class="sub_home">100% gratuita</h3>
            <p class="texto_home">Basta se cadastrar e completar o perfil para se candidatar gratuitamente a qualquer vaga compatível com você.</p>
            <br>
            <h3 class="sub_home">Empresas da região</h3>
            <p class="texto_home">Visamos aproximar e facilitar o contato das empresas da região de Itapira com os alunos da Fatec Itapira.</p>
        </div>
        <div class="container_home_imagem">
            <img src="img/imagem_home.jpg" class="imagem_home">
        </div>
    </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>