<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Login Aluno</title>
</head>
<body>
    <header>
        <a href="./01_home.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>

        <nav>
            <div class="dropdown">
                <button class="dropbtn-home">Cadastrar-se</button>

                <div class="dropdown-content content-sino btn-home">
                    <a href="./06_cadastro_aluno.php">Aluno</a>
                    <a href="./05_cadastro_empresa.php">Empresa</a>
                    <a href="./07_cadastro_fatec.php">Fatec</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn-home">Login</button>
                
                <div class="dropdown-content btn-home">
                    <a href="./03_login_aluno.php">Aluno</a>
                    <a href="./02_login_empresa.php">Empresa</a>
                    <a href="./04_login_fatec.php">Fatec</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="container_principal_login">
            <div class="imagem_lateral_login">
                <h1 class="titulo_pagina_login">Login Aluno</h1>

                <div class="container_img_login">
                    <img src="../img/login_aluno.jpg" class="foto_pag_login">
                </div>
            </div>

            <div class="dados_perfil">
                <div class="card_perfil">
                    <h3 class="subtitulo">Entrar na Plataforma de Vagas</h3>
                    
                    <form action="includes/login_aluno.php" method="post" class="formulario">
                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="email">Login</label>
                                <input type="email" name="email" id="email" class="campo">
                            </div>
                        </div>
                        <br>
                        <div class="sub_formulario">
                            <div class="sub_nome">
                                <label for="senha_login">Senha</label>
                                <input type="password" name="senha_login" id="senha_login" class="campo">
                            </div>
                        </div>
                        <br>
                        <input type="submit" class="btn_login" value="Entrar">
                    </form>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>