<?php
require_once "includes/conexao.php";
require_once "includes/funcoes.php";
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Painel de Vagas</title>
</head>
<body>
    <header>
        <a href="./12_perfil_aluno.php"><img src="../img/logo_fatec.png" class="logo_fatec" id="topo"></a>
        <nav>
            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/sino.png" class="header_icone">
                </button>

                <div class="dropdown-content content-sino">
                    <a href="./15_painel_vagas_aluno.php">Nova vaga cadastrada!</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Parabéns! Você foi aprovado</a>
                    <a href="./09_alterar_perfil_aluno.php">Atualize seu perfil</a>
                </div>
            </div>

            <div class="dropdown">
                <button class="dropbtn">
                    <img src="../img/icones/menu.png" class="header_icone">
                </button>
                
                <div class="dropdown-content">
                    <a href="./15_painel_vagas_aluno.php">Painel de Vagas</a>
                    <a href="./29_minhas_candidaturas_aluno.php">Minhas Candidaturas</a>
                    <a href="./09_alterar_perfil_aluno.php">Alterar Perfil</a>
                    <a href="./01_home.php">Sair</a>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section class="imagem_principal">
            <img src="../img/home-aluno.jpg" class="imagem_topo">
        </section>

        <section class="texto_intro">
            <h2>Decole sua carreira!</h2>
            <p>Este Painel de Vagas é um projeto que a Fatec de Itapira criou para te informar sobre novas oportunidades de emprego.</p>
        </section>

        <section class="area_painel">
            <div class="filtro_lateral">
                <form action="#">
                    <div class="zona">
                        <div class="zona_filtro">
                            <p><strong>Área da vaga</strong></p><br>
                            <input type="checkbox" id="adm" name="adm" value="adm">
                            <label for="adm">Administração</label><br>
                            <input type="checkbox" id="dsm" name="dsm" value="dsm">
                            <label for="dsm">Desenvolvimento de Softwares</label><br>
                            <input type="checkbox" id="gpi" name="gpi" value="gpi">
                            <label for="gpi">Gestão da Produção Industrial</label><br>
                            <input type="checkbox" id="gestao" name="gestao" value="gestao">
                            <label for="gestao">Gestão Empresarial</label><br>
                            <input type="checkbox" id="gti" name="gti" value="gti">
                            <label for="gti">Gestão Tecnologia da Informação</label><br>
                            <input type="checkbox" id="infra" name="infra" value="infra">
                            <label for="infra">Infraestrura de TI</label><br>
                            <input type="checkbox" id="seguranca" name="seguranca" value="seguranca">
                            <label for="seguranca">Segurança da Informação</label><br><br>
                        </div>
                        
                        <div class="zona_filtro">
                            <p><strong>Cidade</strong></p><br>
                            <input type="checkbox" id="campinas" name="campinas" value="campinas">
                            <label for="campinas">Campinas</label><br>
                            <input type="checkbox" id="itapira" name="itapira" value="itapira">
                            <label for="itapira">Itapira</label><br>
                            <input type="checkbox" id="mogi-guacu" name="mogi-guacu" value="mogi-guacu">
                            <label for="mogi-guacu">Mogi Guaçu</label><br>
                            <input type="checkbox" id="mogi-mirim" name="mogi-mirim" value="mogi-mirim">
                            <label for="mogi-mirim">Mogi Mirim</label><br><br>
                        </div>
                    </div>                 

                    <div class="zona">
                        <div class="zona_filtro">
                            <p><strong>Modalidade de trabalho</strong></p><br>
                            <input type="checkbox" id="hibrido" name="hibrido" value="hibrido">
                            <label for="hibrido">Híbrida</label><br>
                            <input type="checkbox" id="home-office" name="home-office" value="home-office">
                            <label for="home-office">Home-office</label><br>
                            <input type="checkbox" id="presencial" name="presencial" value="presencial">
                            <label for="presencial">Presencial</label><br><br>
                        </div>
                        
                        <div class="zona_filtro">
                            <p><strong>Tipo de Contrato</strong></p><br>
                            <input type="checkbox" id="pj" name="pj" value="pj">
                            <label for="pj">Contrato de Trabalho (PJ)</label><br>
                            <input type="checkbox" id="clt" name="clt" value="clt">
                            <label for="clt">Contratação efetiva (CLT)</label><br>
                            <input type="checkbox" id="estagio-ferias" name="estagio-ferias" value="estagio-ferias">
                            <label for="estagio-ferias">Estágio de Férias</label><br>
                            <input type="checkbox" id="estagio-regular" name="estagio-regular" value="estagio-regular">
                            <label for="estagio-regular">Estágio Regular</label><br>
                            <input type="checkbox" id="freelance" name="freelance" value="freelance">
                            <label for="freelance">Projeto Pontual (Freelance)</label><br>
                            <input type="checkbox" id="trainee" name="trainee" value="trainee">
                            <label for="trainee">Trainee</label>
                        </div>
                    </div>
                </form>
            </div>

            <div class="area_cards_vagas">

                <?php

                $busca =  $banco->query("SELECT * FROM vagas");
                if(!$busca){
                    echo "<p>Infelizmente a busca deu errado</p>";
                }
                else{
                    if($busca->num_rows == 0){
                        echo "<p>Nenhum registro encontrado!</p>";
                    }
                    else{
                        while ($reg=$busca->fetch_object()){
                            if($reg->status == 1){
                            $data_limite = data_convert($reg->data_limite);
                            echo"
                <div class='card_vaga'>
                    <div class='head_card_vaga'>
                        <h3 class='titulo_card_vaga'> $reg->titulo</h3>
                        <p class='area_card_vaga'>Área: $reg->area_interesse</p>
                    </div>
                    <div class='body_card_vaga'>
                        <div class='info_card_vaga'>
                            <img src='../img/icones/pin.png' class='icone_card_vaga'>
                            <p class='descricaco_card_vaga'>$reg->modalidade</p>
                        </div>
                        <div class='info_card_vaga'>
                            <img src='../img/icones/bandeira.png' class='icone_card_vaga'>
                            <p class='descricaco_card_vaga'>$reg->cidade_vaga</p>
                        </div>
                        <div class='info_card_vaga'>
                            <img src='../img/icones/pasta.png' class='icone_card_vaga'>
                            <p class='descricaco_card_vaga'>$reg->tipo_vaga</p>
                        </div>
                        <div class='info_card_vaga'>
                            <img src='../img/icones/bolsadinheiro.png' class='icone_card_vaga'>
                            <p class='descricaco_card_vaga'>$reg->faixa_salarial</p>
                        </div>
                        <div class='info_card_vaga'>
                            <img src='../img/icones/calendario.png' class='icone_card_vaga'>
                            <p class='descricaco_card_vaga'>até $data_limite</p>
                        </div>
                        <a href='./18_tela_vaga_aluno.php?id_vaga=$reg->id_vaga' class='link_vaga'>+ informações</a>
                    </div>
                </div>";
                    }
                    }
                }
                }
                ?>
                <?php
                    $banco->close();
                ?>
            </div>
        </section>
    </main>

    <footer>
        <div class="texto_footer">
            <h3>Dúvidas ou Sugestões? Converse com a gente!</h3>
        </div>

        <div class="info_contatos">
            <p>contato@fatecitapira.edu.br</p>
            <p>(19) 3843-1996</p>
            <p><a href="https://www.fatecitapira.edu.br/" target="_blank">www.fatecitapira.edu.br</a></p>
        </div>

        <div class="icones_footer">
            <a href="https://www.instagram.com/accounts/login/?next=/fatecdeitapira/" target="_blank"><img src="../img/icones/instagram.png" class="icon_footer"></a>
            <a href="https://pt-br.facebook.com/fatecitapira/" target="_blank"><img src="../img/icones/facebook.png" class="icon_footer"></a>
            <a href="https://api.whatsapp.com/send?phone=551938635210" target="_blank"><img src="../img/icones/whatsapp.png" class="icon_footer"></a>

        </div>

        <a href="#topo" class="backtotop">
            <img src="../img/icones/foguete.png" class="foguete">
            <p>Voltar ao topo</p>
        </a>
    </footer>
</body>
</html>